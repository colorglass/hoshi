#pragma once

#include <gluino/string_cast.h>

#include "concepts.h"
#include "message_error.h"

namespace hoshi {
    template <::hoshi::message_source Source>
    class message {
    public:
        using source_type = Source;
        using value_type = typename Source::value_type;
        using traits_type = typename Source::traits_type;
        using allocator_type = typename Source::allocator_type;
        using string_type = ::std::basic_string<value_type, traits_type, allocator_type>;
        using string_view_type = ::std::basic_string_view<value_type, traits_type>;

        message() = default;

        inline explicit message(::std::string_view msg) : _key(msg.data()) {}

        template <class CharOut, class... Args>
        [[nodiscard]] inline string_view_type format(std::size_t count = 1, Args... args) const {
            return ::std::format(str<CharOut>(count), args...);
        }

        template <class CharOut, class... Args>
        [[nodiscard]] inline string_view_type format(std::size_t count, const ::std::locale&& loc, Args... args) const {
            return ::std::format(str<CharOut>(count, loc), args...);
        }

        template <class... Args>
        [[nodiscard]] inline string_view_type format(std::size_t count = 1, Args... args) const {
            return ::std::format(str(count), args...);
        }

        template <class... Args>
        [[nodiscard]] inline string_view_type format(std::size_t count, const ::std::locale&& loc, Args... args) const {
            return ::std::format(str(count, loc), args...);
        }

        template <class CharOut>
        [[nodiscard]] inline auto str(::std::size_t count = 1) const {
            return str<CharOut>(count, ::std::locale());
        }

        template <class CharOut>
        [[nodiscard]] inline auto str(::std::size_t count, const ::std::locale&& loc) const {
            return str<CharOut>(count, loc);
        }

        template <class CharOut>
        [[nodiscard]] inline auto str(::std::size_t count, const ::std::locale& loc) const {
            return ::gluino::string_cast<CharOut>(str(count, loc));
        }

        [[nodiscard]] inline string_view_type str(::std::size_t count = 1) const {
            return str(count, ::std::locale());
        }

        [[nodiscard]] inline string_view_type str(::std::size_t count, const ::std::locale&& loc) const {
            return str(count, loc);
        }

        [[nodiscard]] string_view_type str(::std::size_t count, const ::std::locale& loc) const {
            if (_key.empty()) {
                return "";
            }
            auto result = Source{}.get(_key, count, loc);
            if (!result.has_value()) {
                throw ::hoshi::message_error(_key);
            }
            return result.value();
        }

    private:
        string_type _key;
    };
}
