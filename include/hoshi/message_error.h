#pragma once

#include <format>
#include <stdexcept>

namespace hoshi {
    class message_error : public ::std::logic_error {
    public:
        inline message_error() noexcept : ::std::logic_error("Could not find message.") {}

        inline explicit message_error(::std::string_view message)
            : ::std::logic_error(::std::format("Could not find message '{}'.", message)), _message(message.data()) {}

        [[nodiscard]] inline std::string_view message() const noexcept {
            return _message;
        }

    private:
        ::std::string _message;
    };
}  // namespace hoshi