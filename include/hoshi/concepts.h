#pragma once

namespace hoshi {
    template <class Source>
    concept message_source = requires(const Source& value, ::std::string_view message, ::std::size_t count,
                                      const ::std::locale& locale) {
        typename Source::value_type;
        typename Source::traits_type;
        typename Source::allocator_type;

        { value.get(message) } -> ::std::convertible_to<::std::optional<::std::string_view>>;
        { value.get(message, count) } -> ::std::convertible_to<::std::optional<::std::string_view>>;
        { value.get(message, locale) } -> ::std::convertible_to<::std::optional<::std::string_view>>;
        { value.get(message, count, locale) } -> ::std::convertible_to<::std::optional<::std::string_view>>;
    };
}
